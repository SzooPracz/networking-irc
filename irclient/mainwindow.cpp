#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <queue>
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define MAX_MESSAGE_LENGTH 100
#define REFRESH_RATE 1000 // [ms] okres wyswietlania wiadomości zapisanych przez ReaderThread
using std::string;
using std::queue;

//struktura zawierająca dane, które zostaną przekazane do wątku
struct thread_data_t
{
    int sockd;
    pthread_mutex_t *messages_lock; // mutex dla kolejki wiadomości
    queue <string> *messages; // kolejka odebranych wiadomości do przekazania do gui
};

void * ReaderThreadBehavior(void *t_data)
{

    pthread_detach(pthread_self());
    struct thread_data_t *th_data = (struct thread_data_t *) t_data;
    std::cout<<"receiver started\n";
    while(1) {

        try{
            char message_ar[MAX_MESSAGE_LENGTH];
            memset(message_ar, '\0', sizeof(char) * MAX_MESSAGE_LENGTH);
            ssize_t read_result = read((*th_data).sockd, message_ar, MAX_MESSAGE_LENGTH - 1);

            if (read_result == 0) break; // jeśli rozłączono, zakończ
            string message(message_ar);


            std::cout << "received : " + message + "\n";
            pthread_mutex_lock((*th_data).messages_lock);
            (*th_data).messages->push(message);
            pthread_mutex_unlock((*th_data).messages_lock);
        }catch(...){
            std::cout<<"couldn't read message\n";
        }
    }
    std::cout<<"thread exits\n";
    pthread_exit(nullptr);
}

int MainWindow::startConnection(std::string server_str, std::string port_str){

    // setup wątku read
    //todo request rooms list

    char port[port_str.size()+1];
    memset(port,'\0', sizeof(char)*(port_str.size()+1));
    memcpy(port,port_str.c_str(),port_str.size());

    char server[server_str.size()+1];
    memset(server, '\0',sizeof(char)*(server_str.size()+1));
    memcpy(server,server_str.c_str(),server_str.size());


    int connection_socket_descriptor;
    int connect_result;
    struct sockaddr_in server_address;
    struct hostent* server_host_entity;



    server_host_entity = gethostbyname(server);
    if (! server_host_entity)
    {
            fprintf(stderr, "Nie można uzyskać adresu IP serwera.\n");
            return -1;
    }

    connection_socket_descriptor = socket(PF_INET, SOCK_STREAM, 0);
    if (connection_socket_descriptor < 0)
    {
        fprintf(stderr, "Błąd przy probie utworzenia gniazda.\n");
        return -1;
    }
    socket_desc = connection_socket_descriptor;
    memset(&server_address, 0, sizeof(struct sockaddr));
    server_address.sin_family = AF_INET;
    memcpy(&server_address.sin_addr.s_addr, server_host_entity->h_addr, server_host_entity->h_length);
    server_address.sin_port = htons(atoi(port));

    connect_result = ::connect(connection_socket_descriptor, (struct sockaddr*)&server_address, sizeof(struct sockaddr));
    if (connect_result < 0)
    {
        fprintf(stderr, "Błąd przy próbie połączenia z serwerem.\n");
        return -1;
    }



    //uchwyt na wątek
    pthread_t reader_thread;

    //dane, które zostaną przekazane do wątku

    struct thread_data_t *t_data = new thread_data_t {
        connection_socket_descriptor,
        messages_lock,
        messages
    };
    int create_result = pthread_create(&reader_thread, nullptr, ::ReaderThreadBehavior, (void *)t_data);
    if (create_result){
        printf("Błąd przy próbie utworzenia wątku, kod błędu: %d\n", create_result);
        return -1;
    }

    return 0;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    socket_desc(0),
    messages_lock( new pthread_mutex_t(PTHREAD_MUTEX_INITIALIZER)),
    messages(new queue<string>),
    ui(new Ui::MainWindow),
    timer(new QTimer(this))
{
    ui->setupUi(this);
    ui->textBrowser->setText("not connected");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_send_button_clicked()
{
    // wyślij wiadomość z lineEdit
    QString message = ui->lineEdit->displayText();
    if (message.isEmpty()) return;
    ui->lineEdit->clear();
    // zamienia QString message na char[]
    char message_ar[MAX_MESSAGE_LENGTH];
    memset(message_ar,'\0', sizeof(char)*MAX_MESSAGE_LENGTH);
    int length = message.length();
    if (length > MAX_MESSAGE_LENGTH)
        length = MAX_MESSAGE_LENGTH;
    memcpy(message_ar,message.toStdString().c_str(),length);

    ssize_t write_result = write(socket_desc,message_ar,length);
    if (write_result>0){
        std::cout<<"sent a message: \n"<<message.toStdString()<<"\n";
    }

}

void MainWindow::refresh(){
    // pokazuje pobrane wiadomości na ekranie (textBrowser)
    // wywoływane automatycznie cyklicznie
    std::cout<<"refreshing..\n";
    pthread_mutex_lock(messages_lock);
    while(!(*messages).empty()){

        QString message = QString::fromStdString((*messages).front());
        (*messages).pop();
        ui->textBrowser->append(message);
    }
    pthread_mutex_unlock(messages_lock);
}

void MainWindow::on_refresh_button_clicked()
{
    // refresh rooms
}

void MainWindow::on_nick_button_clicked()
{
    // set nickname
//    ui->lineEditNick
}

void MainWindow::on_connect_button_clicked()
{
    // startConnection (host, port)
    // adres serwera i port pobierz z inputów
    string server = ui->lineEditServer->displayText().toStdString();
    string port = ui->lineEditPort->displayText().toStdString();
    // połącz z serwerem - jeśli pomyślnie połączono , to odblokuj input
    int isConnected = startConnection(server,port);
    if (isConnected == 0){
        ui->textBrowser->clear();
        ui->lineEdit->setEnabled(true);
        ui->send_button->setEnabled(true);
        ui->connect_button->setEnabled(false);
        //timer zamiast main loop do wyswietlania nowych wiadomości
        connect(timer, SIGNAL(timeout()), this, SLOT(refresh()));
        timer->start(REFRESH_RATE); // co (sekunde) odśwież wiadomości
    }

}
