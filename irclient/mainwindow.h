#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QTimer>
#include <queue>
#include <string>
#include <vector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    static void * ReaderThreadBehavior(void *t_data);
    int startConnection(std::string server_str, std::string port_str);
    ~MainWindow();

private slots:
    void on_send_button_clicked();
    void refresh();

    void on_refresh_button_clicked();

    void on_nick_button_clicked();

    void on_connect_button_clicked();

private:
    int socket_desc;
    pthread_mutex_t *messages_lock;
    std::queue<std::string> *messages;
    std::vector<std::string> rooms;
    std::string active_room;
    Ui::MainWindow *ui;
    QTimer *timer;
};

#endif // MAINWINDOW_H
