#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <string>
#include <queue>
#include <cstring>
#include <iostream>
#include <set>
#include <vector>

#include <signal.h>
#define SERVER_PORT 1234
#define MAX_MESSAGE_LENGTH 100
#define QUEUE_SIZE 5
using std::string;
using std::queue;
using std::set;
using std::pair;
using std::vector;

// todo port jako parametr
// todo - zamykanie

//struktura zawierająca dane, które zostaną przekazane do wątku
struct thread_data_t
{
    int sockd;
    // wskaznik na strukture w której bedą wiadomości odebrane od klientów
    pthread_mutex_t *messages_lock; // mutex dla kolejki wiadomości
    queue <string> *messages_store; // kolejka wiadomości do wysłania do klientów

    pthread_mutex_t *listeners_lock;
    set <int> *active_listeners; // deskryptory do wysyłania wiadomośc

    pthread_mutex_t *rooms_lock;
    vector< pair<string, set<int>>> *rooms; // każda nazwa pokoju połączona ze zbiorem klientów (deskryptorów)

};

//watek odbierajacy dane od klienta, dla kazdego klienta nowy watek
void *ReaderThreadBehavior(void *t_data)
{
    std::cout<< "receiver started!\n";
    pthread_detach(pthread_self());
    struct thread_data_t *th_data = (struct thread_data_t*)t_data;

    while (1){
        char message_ar[MAX_MESSAGE_LENGTH];
        memset(message_ar,'\0', sizeof(char)*MAX_MESSAGE_LENGTH);
        ssize_t read_result = read((*th_data).sockd, message_ar,MAX_MESSAGE_LENGTH-1);
        if (read_result <= 0) break; // jeśli rozłączono, zakończ

        string message(message_ar);
        //sprawdzic do jakiego pokoju ma trafić wiadomość
        // [pokoj] wiadomosc
        // /join pokoj <- nie wysylac, ale dodac usera do pokoju
        if (message.substr(0,5) == "/join"){
            unsigned long find_result = message.find(' ',6);
            if (find_result > 0) {

                string room_name = message.substr(6, find_result-1);
                std::cout<<"user"<<(*th_data).sockd<<" wants to join room: "<< room_name<<std::endl;
                pthread_mutex_lock((*th_data).rooms_lock);
                for (unsigned int i=0;i<(*th_data).rooms->size();i++){
                    if ((*th_data).rooms->at(i).first== room_name){
                        // dodaj do pokoju
                        (*th_data).rooms->at(i).second.insert((*th_data).sockd);
                        std::cout<<"user"<<(*th_data).sockd<<" joined room: "<< room_name<<std::endl;

                        break;
                    }
                }
                pthread_mutex_unlock((*th_data).rooms_lock);
            }
            // jeśli podano '/join' to nie jest to wiadomosc; idz dalej
            continue;
        }


        //dopisz usera na początku wiadomości <- to wymaga zamiany od kiedy [pokój] jest na początku wiadomości
//        message = "user" + std::to_string((*th_data).sockd) + "  " + message;

        // dodaj wiadomość do wspolnej kolejki
        pthread_mutex_lock((*th_data).messages_lock);
        (*th_data).messages_store->push(message);
        pthread_mutex_unlock((*th_data).messages_lock);
    }

    close((*th_data).sockd);

    // usuń descriptor ze zbioru listeners
    pthread_mutex_lock((*th_data).listeners_lock);
    (*th_data).active_listeners->erase((*th_data).sockd);
    pthread_mutex_unlock((*th_data).listeners_lock);
    // zwolnienie pamieci - kazdy wątek ma swoja strukture
    delete th_data;
    pthread_exit(nullptr);
}

void * SenderThreadBehavior(void *t_data){
    //todo empty message check
    pthread_detach(pthread_self());
    struct thread_data_t *th_data = (struct thread_data_t*)t_data;

    std::cout<<"sender started!\n";
    while (1) {
        sleep(1);

        // wez jedna wiadomosc z kolejki
        pthread_mutex_lock((*th_data).messages_lock);
        if ((*th_data).messages_store->empty()) {
            pthread_mutex_unlock((*th_data).messages_lock);
//          no messages to send
            continue;
        }
        string message;
        message = (*th_data).messages_store->front();
        (*th_data).messages_store->pop();
        pthread_mutex_unlock((*th_data).messages_lock);
        std::cout<< "message: '"<< message << "'" << std::endl;

        // sprawdź pokój docelowy w '[pokój] wiadomosć'
        int find_begin, find_end;
        find_begin = message.find("[");
        find_end = message.find("]",find_begin);
        string room_name = "";

        if (find_begin >= 0 and find_end > 0 and find_begin < find_end){
            // jeśli znaleziono nawiasy kwadratowe to wytnij nazwe pokoju
            room_name = message.substr(find_begin+1,find_end-1);
        }

        //convert to char []
        char message_ar[MAX_MESSAGE_LENGTH];
        memset(message_ar,'\0', sizeof(char)*MAX_MESSAGE_LENGTH);
        strncpy(message_ar,message.c_str(), message.length());
        if (room_name != ""){ // podano pokoj, wyslij do wszystkich w pokoju
            std::cout<<"message to room: "<<room_name<<std::endl;
            pthread_mutex_lock((*th_data).rooms_lock);
            set <int> clients;
            for (unsigned int i=0;i<(*th_data).rooms->size();i++){
                if ((*th_data).rooms->at(i).first== room_name){
                    // pokoj odnaleziony
                    //skopiuj członków pokoju do set clients
                    clients = set<int>((*th_data).rooms->at(i).second);
                    break;
                }
            }
            pthread_mutex_unlock((*th_data).rooms_lock);
            // wyslij wiadomosc do wszystkich w clients
            set<int>::iterator it;
            //wyslij wiadomosc do wszystkich
            for (it = clients.begin(); it != clients.end(); ++it) {
                int write_result = write(*it, message_ar, message.length());
                if (write_result <= 0) {
                    // todo usuwać nieaktywne klienty ze zbiorów!
                }
            }
        }
        if (room_name == "") { // nie podano pokoju, wyslij do wszystkich active_listeners
            // todo do usunięcia po skończeniu systemu pokoi
            //skopiuj liste deskryptorów klientów do wysłania wiadomości
            pthread_mutex_lock((*th_data).listeners_lock);
            set<int> listeners(*((*th_data).active_listeners));
            pthread_mutex_unlock((*th_data).listeners_lock);

            set<int>::iterator it;
            //wyslij wiadomosc do wszystkich
            for (it = listeners.begin(); it != listeners.end(); ++it) {
                int write_result = write(*it, message_ar, message.length());
                if (write_result <= 0) {
                    // usun listenera z active listeners
                    pthread_mutex_lock((*th_data).listeners_lock);
                    (*th_data).active_listeners->erase(*it);
                    pthread_mutex_lock((*th_data).listeners_lock);
                }
            }
        }

    }
    pthread_exit(NULL);
}

void close_app(sig_t s){ // NOT USED
    // todo close all sockets
    // stop SenderThread
    std::cout<<"ctrl + c pressed, closing..\n";


}
int main(int argc, char* argv[])
{
    int server_socket_descriptor;
    int connection_socket_descriptor;
    int bind_result;
    int listen_result;
    char reuse_addr_val = 1;


    queue <string> messages; // wspólne wiadomości
    set <int> active_listeners; // aktywne klienty (odbiorcy wiadomości)
    vector <pair<string,set<int>>>  rooms;
    pthread_mutex_t messages_lock = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t listeners_lock = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t rooms_lock = PTHREAD_MUTEX_INITIALIZER;

    // init 4 pokoje
    rooms.push_back(pair<string,set<int>>("room1",set<int>()));
    rooms.push_back(pair<string,set<int>>("room2",set<int>()));
    rooms.push_back(pair<string,set<int>>("room3",set<int>()));
    rooms.push_back(pair<string,set<int>>("room4",set<int>()));


    struct sockaddr_in server_address;

    //inicjalizacja gniazda serwera
    memset(&server_address, 0, sizeof(struct sockaddr));
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(SERVER_PORT);

    server_socket_descriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (server_socket_descriptor < 0)
    {
        fprintf(stderr, "%s: Błąd przy próbie utworzenia gniazda..\n", argv[0]);
        exit(1);
    }
    setsockopt(server_socket_descriptor, SOL_SOCKET, SO_REUSEADDR, (char*)&reuse_addr_val, sizeof(reuse_addr_val));

    bind_result = bind(server_socket_descriptor, (struct sockaddr*)&server_address, sizeof(struct sockaddr));
    if (bind_result < 0)
    {
        fprintf(stderr, "%s: Błąd przy próbie dowiązania adresu IP i numeru portu do gniazda.\n", argv[0]);
        exit(1);
    }

    listen_result = listen(server_socket_descriptor, QUEUE_SIZE);
    if (listen_result < 0) {
        fprintf(stderr, "%s: Błąd przy próbie ustawienia wielkości kolejki.\n", argv[0]);
        exit(1);
    }



    //uchwyt na wątek
    pthread_t sender_thread;
    //dane, które zostaną przekazane do wątku
    //dynamiczne utworzenie instancji struktury thread_data_t o nazwie t_data
    // TODO (+ w odpowiednim miejscu zwolnienie pamięci)
    thread_data_t *sender_data = new thread_data_t{
            .sockd = 0,
            .messages_lock = &messages_lock,
            .messages_store = &messages,
            .listeners_lock = &listeners_lock,
            .active_listeners = &active_listeners,
            .rooms_lock = &rooms_lock,
            .rooms = &rooms
    };
    // jeden wątek wysyłający wiadomości
    int create_result = pthread_create(&sender_thread, nullptr, SenderThreadBehavior, (void *) sender_data);
    if (create_result){
        printf("Błąd przy próbie utworzenia wątku, kod błędu: %d\n", create_result);
        exit(-1);
    }

    // złapanie ctrl+c, wywołanie close_app()
//    signal(SIGINT, reinterpret_cast<__sighandler_t>(&close_app));
    while(1)
    {
        connection_socket_descriptor = accept(server_socket_descriptor, NULL, NULL);
        if (connection_socket_descriptor < 0)
        {
            fprintf(stderr, "%s: Błąd przy próbie utworzenia gniazda dla połączenia.\n", argv[0]);
            exit(1);
        }

        // dodaj descriptor do zbioru active_listeners
        pthread_mutex_lock(&listeners_lock);
        active_listeners.insert(connection_socket_descriptor);
        pthread_mutex_unlock(&listeners_lock);

        //  dla każdego klienta nowy wątek odbierający wiadomości
        //wynik funkcji tworzącej wątek
        //uchwyt na wątek
        pthread_t thread;
        //dane, które zostaną przekazane do wątku
        //dynamiczne utworzenie instancji struktury thread_data_t o nazwie t_data
        // TODO (+ w odpowiednim miejscu zwolnienie pamięci)

        thread_data_t *t_data = new thread_data_t{
                .sockd = connection_socket_descriptor,
                .messages_lock = &messages_lock,
                .messages_store = &messages,
                .listeners_lock = &listeners_lock,
                .active_listeners = &active_listeners,
                .rooms_lock = &rooms_lock,
                .rooms = &rooms
        };
        create_result = pthread_create(&thread, nullptr, ReaderThreadBehavior, (void *) t_data);
        if (create_result){
            printf("Błąd przy próbie utworzenia wątku, kod błędu: %d\n", create_result);
            exit(-1);
        }
    }
    delete sender_data;
    close(server_socket_descriptor);
    return(0);
}
